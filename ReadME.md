## Sudoku Genetic Algorithm  

The program simulates a population of individuals trying to obtain the best fitness value - solve the sudoku puzzle.

The outcomes of a simulation can be seen on the figure below.  
 1) Showing the fitness of the fittest individuals and children born per generation.
 2) Showing the Adaptive Mutation Rate changing through generations.

<div align="center">
<img src="fig1.png"></img>
</div>

The input file `sudoku_example.txt` contains a flatten 9x9 sudoku filled with known digits 1..9 and unknown = 0.  
You can run the file on your own using command `python sudoku.py`.

<div align="center">
<img src="showcase.gif"></img>
</div>