from __future__ import annotations
from typing import List, Tuple, Dict
Success = bool

import numpy as np
import pandas as pd
from collections import Counter
import matplotlib.pyplot as plt
import seaborn as sns

import random
random.seed(0)
np.random.seed(0)


def decision(probability: float) -> bool:
    """Return true/false with some probability."""
    return random.random() < probability

def count_duplicates(array) -> int:
    """Count number of duplicated values in a 1D array."""
    digit_counts = list(Counter(array).values())
    return sum(digit_counts) - len(digit_counts)


class Individual(object):
    def __init__(self, genes: np.array) -> None:
        self.genes = genes
        self.update_fitness()
        
    def update_fitness(self) -> int:
        """The fitness of an individual is determined by how close it is to the puzzle result (no duplications in rows, columns, 9 3x3 chunks)."""
        dups = 0
        
        for row in self.genes:
            dups += count_duplicates(row)
            
        for col in self.genes.T:
            dups += count_duplicates(col)
            
        for x in range(0, 7, 3):
            for y in range(0, 7, 3):
                chunk = self.genes[x:x+3, y:y+3]
                dups += count_duplicates(chunk.flatten())
        
        self.fitness = (216 - dups) / 216
        
    def mutate(self, mutation_rate: float, given: Given) -> Success:
        """Mutate an individual by picking a row, and then two columns to swap."""
        if decision(mutation_rate):
            while True:
                row = np.random.randint(0, 9)
                column1, column2 = np.random.choice(range(9), size=2, replace=False)
                
                # Check if both of selected genes are free...
                if given.genes[row, column1] == 0 and given.genes[row, column2] == 0:
                    # ...and that we are not duplicating any value.
                    if(not given.is_column_duplicate(column1, self.genes[row, column2])
                       and not given.is_column_duplicate(column2, self.genes[row, column1])
                       and not given.is_chunk_duplicate(row, column2, self.genes[row, column1])
                       and not given.is_chunk_duplicate(row, column1, self.genes[row, column2])):
                        
                        # Swap genes
                        self.genes[row, column1], self.genes[row, column2] = self.genes[row, column2], self.genes[row, column1]
                        return True
                        
        return False
    
    
class Given(Individual):
    """Stores the state of original sudoku. Known values are numbers [1, 9], unknown are 0's."""
    def __init__(self, path: str) -> None:
        self.genes = np.loadtxt(path, dtype=int).reshape(9, 9)
        self.update_fitness()
        
    def is_row_duplicate(self, row: int, value: int) -> bool:
        """Check if the value doesn't already exist in the following row."""
        return value in self.genes[row]

    def is_column_duplicate(self, column: int, value: int) -> bool:
        """Check if the value doesn't already exist in the following column."""
        return value in self.genes[:, column]

    def is_chunk_duplicate(self, row: int, column: int, value: int) -> bool:
        """Check if the value doesn't already exist in the following chunk."""
        i = 3 * (row // 3)
        j = 3 * (column // 3)
        return value in self.genes[i:i+3, j:j+3]

    
class Sudoku(object):
    """Solves the given sudoku puzzle using Genetic Algorithm."""
    def __init__(self, given: Given, population_size: int, crossover_rate: float, mutation_rate: float) -> None:
        self.given = given
        self.population_size = population_size
        self.crossover_rate = crossover_rate
        self.mutation_rate = mutation_rate
        self.population = None
    
    
    def select(self, candidates: List[Individual]) -> Tuple[Individual]:
        """Two parents are chosen from 100 best candidates. The fittest have higher chances of reproduction."""
        survived = candidates[:100]
        
        chances = [individual.fitness for individual in survived]
        parent1, parent2 = random.choices(survived, chances, k=2)
        
        return parent1, parent2
    
    def crossover(self, parent1: Individual, parent2: Individual) -> Individual:
        """Two parents create an offspring either by exchanging their genes, or copying ones."""
        gs1, gs2 = parent1.genes, parent2.genes
        
        if decision(probability=self.crossover_rate):
            # Crossover points are chosen at random from within the genes
            cp1, cp2 = np.random.choice(range(9), size=2, replace=False)
            if cp1 > cp2: cp1, cp2 = cp2, cp1
            
            offspring_genes = np.concatenate([gs1[:cp1], gs2[cp1:cp2], gs1[cp2:]], axis=0)
            
        else:
            offspring_genes = random.choice([gs1, gs2])
            
        return Individual(offspring_genes)
        
        
    def seed_population(self) -> None:
        """Create population by filling copies of Given sudoku with digits in random order, not duplicating any digit per row."""
        self.population = []
        
        for _ in range(self.population_size):
            sudoku = np.zeros(shape=(9, 9), dtype=int)
            
            for x, row in enumerate(self.given.genes):
                free_digits = list({0, 1, 2, 3, 4, 5, 6, 7, 8, 9} - set(row))
                for y, value in enumerate(row):
                    if value == 0:
                        digit = random.choice(free_digits)
                        sudoku[x, y] = digit
                        free_digits.remove(digit)
                    else:
                        sudoku[x, y] = value
                        
            new_individual = Individual(sudoku)
            new_individual.update_fitness()
            self.population.append(new_individual)
            
               
    def solve(self, max_generations: int) -> Tuple[Individual, Dict[set,set,set]]:
        history = {'fittest': set(), 'offspring': set(), 'AMR': set()}
        Np = self.population_size # Number of individuals
        Nm = 0 # Number of mutations (resets per generation)
        Nf = int(Np * 0.05) # Number of fittest individuals
        
        # Mutation variables
        mutation_rate = self.mutation_rate
        phi = 0
        sigma = 1
            
        self.seed_population()
        last_best = self.given
        stale = 1
        
        for generation in range(max_generations):
            # Monitor and broadcast fitness values info
            best = max(self.population, key=lambda ind: ind.fitness)
            if best.fitness > last_best.fitness:
                print(f'\nBEST FITNESS :: {best.fitness}', end='')
                last_best = best
                stale = 0
            else:
                stale += 1
                print(f'\rBEST FITNESS :: {best.fitness} (x{stale})', end='')
            
            if best.fitness == 1.0:
                return best, history
                

            # Create the next population
            next_population = []
            
            # Select the fittest individuals and preserve them for next generation
            # They will not be affected by crossover or mutation
            self.population.sort(key=lambda x: x.fitness, reverse=True)
            fittest = self.population[:Nf]
            next_population.extend(fittest)
            
            # Produce offsprings for next population
            for _ in range(Nf, Np):
                parent1, parent2 = self.select(self.population)
                offspring = self.crossover(parent1, parent2)
                success = offspring.mutate(self.mutation_rate, self.given)
                offspring.update_fitness()
                next_population.append(offspring)
                
                # Report history
                if success:
                    history['offspring'].add((generation, offspring.fitness))
                    Nm += 1
                    if (offspring.fitness > best.fitness):
                        phi += 1
                        
            # Calculate adaptive mutation rate (based on Rechenberg's 1/5 success rule). 
            # This is to stop too much mutation as the fitness progresses towards unity.
            try:
                phi /=  Nm
            except ZeroDivisionError:
                phi = 0
            
            if phi > 0.2:
                sigma /= 0.998
            if phi < 0.2:
                sigma *= 0.998

            mutation_rate = abs(np.random.normal(loc=0, scale=sigma, size=None))
            Nm = 0
            phi = 0
            
            # Report history
            history['AMR'].add((generation, mutation_rate))
            history['fittest'].update(set([(generation, i.fitness) for i in fittest]))
            
            
            self.population = next_population
        else:
            raise 'ERROR :: Solution not found'
            
                
def main(save_history=True) -> None:
    example = Given('sudoku_example.txt')
    print('GIVEN SUDOKU:')
    print(example.genes)
    
    sudoku = Sudoku(
        given=example,
        population_size=3000,
        crossover_rate=0.6,
        mutation_rate=0.06
    )
    
    solved, history = sudoku.solve(max_generations=300)
    print(f'\n\nSUDOKU SOLVED!\n{solved.genes}')
    
    if save_history:
        with open('history.txt', 'w') as file:
            file.write('generation,type,value\n')
            for generation, fitness in history['fittest']:
                file.write(f'{generation},fittest,{fitness}\n')
            for generation, fitness in history['offspring']:
                file.write(f'{generation},offspring,{fitness}\n')
            for generation, mutation_rate in history['AMR']:
                file.write(f'{generation},AMR,{mutation_rate}\n')
        
            
def plot_history() -> None:
    df = pd.read_csv('history.txt')
    fit = df[df['type'] == 'fittest']
    ofs = df[df['type'] == 'offspring']
    amr = df[df['type'] == 'AMR']
    amr = amr.sort_values(by='generation')
    
    Ng = df.generation.max()
    y = np.arange(Ng + 1)
    
    fig, axes = plt.subplots(1, 2, figsize=(10, 5))
    axes[0].scatter(ofs.generation, ofs.value, c='g')
    axes[0].scatter(fit.generation, fit.value, c='y')
    axes[0].set_xlabel('Generation number')
    axes[0].set_ylabel('Level of fitness')
    axes[0].set_title('Sudoku chromosomes evolution')
    axes[0].legend(['Offsprings', 'Fittest individuals'])
    
    axes[1].bar(y, amr.value)
    axes[1].set_xlabel('Generation number')
    axes[1].set_ylabel('Adaptive mutation rate')
    axes[1].set_title('Adaptive Mutation Rate vs. Generation')
    plt.xticks(y, amr.generation)
    
    plt.savefig('fig1.png', dpi=500)
    plt.show()

            
if __name__ == '__main__':
    main(save_history=False)
    # plot_history() 